#!/usr/bin/env python3

## Copyright (C) 2023 David Miguel Susano Pinto <pinto@robots.ox.ac.uk>
##
## Copying and distribution of this file, with or without modification,
## are permitted in any medium without royalty provided the copyright
## notice and this notice are preserved.  This file is offered as-is,
## without any warranty.

## NAME
##
##   lisa-project-from-filelist - Create a LISA project from a list of filepaths
##
## SYNPOSIS
##
##   lisa-project-from-filelist \
##       [--prefix FILE-PREFIX] \
##       [--name PROJECT-NAME] \
##       [--data-dir DATA-DIR] \
##       FILELIST
##
## DESCRIPTION
##
##   This script prints a LISA project given a list of filepaths in
##   FILELIST.  The FILELIST file should have one filepath per line,
##   either absolute paths or relative to both DATA-DIR and
##   FILE-PREFIX.  If the filepaths are relative, the FILE-PREFIX
##   specify the prefix for LISA to access the files (typically a URL)
##   while DATA-DIR specify the prefix for this program to access the
##   files.
##
##   This script does not provide options to add file or region
##   attributes because such options would become too complicated and
##   can easily be accomplished with `jq' (see ADDING ATTRIBUTES WITH
##   JQ section).
##
## OPTIONS
##
##   --prefix FILE-PREFIX
##
##       Images in LISA projects can be prefixed with a fixed string
##       (can be changed in LISA with the keyboard shortcut "f").
##       This defaults to the empty string.
##
##       This is useful in the typical case where all images share the
##       initial path.  This makes the project file smaller while also
##       making it easier to fix when the data moves location.
##
##       Note that this a *prefix* and will be prepended to the
##       individual files lines by LISA itself.  A missing file
##       separator is not automatically added at the end of it.
##
##   --name PROJECT-NAME
##
##       This specifies the project name which is displayed in the top
##       bar / header of LISA.  Defaults to the empty string.
##
##   --data-dir DATA-DIR
##
##       A LISA project includes information about the image size.  In
##       order to get that information, this program needs access to
##       the images.  This option defines the path to which FILELIST
##       is relative to.  Defaults to the current directory.
##
## EXAMPLES
##
##     find /scratch/shared/beegfs/me/project-data \
##         -type f \
##         | sed 's,^/scratch/shared/beegfs/me/project-data/,,'
##         > data-manifest
##     lisa-project-from-filelist \
##         --prefix https://meru.robots.ox.ac.uk/dset/image/some-project/ \
##         --name "An amazing project" \
##         --data-dir /scratch/shared/beegfs/me/project-data \
##         data-manifest
##
## ADDING ATTRIBUTES WITH JQ
##
##   Adding attributes to a LISA project can be done trivially with
##   `jq', the only tricky part is knowing the LISA format.  Here's an
##   example that adds a region radio attribute and a file label
##   attribute:
##
##       lisa-project-from-filelist \
##           ... \
##           | jq '.attributes.region.bbox = {
##                 "atype": "radio",
##                 "aname": "Box",
##                 "options": {
##                     "option1": "Option #1",
##                     "option2": "Option #2"
##                 },
##                 "default_option_id": "option2"
##             }
##             | .config.show_attributes.region |= . + ["bbox"]
##             | .attributes.file.title = {
##                 "atype": "label",
##                 "aname": "Title"
##             }
##             | .config.show_attributes.file |= . + ["title"]' \
##           > lisa-project.json
##


import argparse
import json
import os.path
import sys
import time
import uuid
from typing import List

import PIL.Image


def create_empty_lisa_project():
    if hasattr(time, "time_ns"):  # would required Python 3.7
        timestamp = time.time_ns() // 10**6
    else:
        timestamp = int(time.time() * 10**3)

    return {
        "attributes": {
            "file": {
                "height": {
                    "atype": "text",
                    "aname": "Height",
                },
                "width": {
                    "atype": "text",
                    "aname": "Width",
                },
            },
            "region": {},
        },
        "config": {
            "navigation_from": 0,
            "file_src_prefix": "",
            "item_height_in_pixel": 1200,
            "item_per_page": -1,
            "show_attributes": {
                "file": [],
                "region": [],
            },
            "navigation_to": 500,
            "float_precision": 4,
        },
        "files": [],
        "project": {
            "created_timestamp": timestamp,
            "file_format_version": "0.0.3",
            "project_id": str(uuid.uuid1()),
            "project_name": "",
            "shared_fid": "__FILE_ID__",
            "shared_rev": "__FILE_REV_ID__",
            "shared_rev_timestamp": "__FILE_REV_TIMESTAMP__",
        },
    }


def main(argv: List[str]) -> int:
    argv_parser = argparse.ArgumentParser()
    argv_parser.add_argument("--prefix", default="")
    argv_parser.add_argument("--name", default="")
    argv_parser.add_argument("--data-dir", default="")
    argv_parser.add_argument(
        "filelist",
        help="Text file with image file paths (relative to DATA_DIR)",
    )
    args = argv_parser.parse_args(argv[1:])

    lisa_project = create_empty_lisa_project()
    lisa_project["project"]["project_name"] = args.name
    lisa_project["config"]["file_src_prefix"] = args.prefix

    with open(args.filelist, "r") as fh:
        for fid, line in enumerate(fh):
            src = line.strip()
            local_fpath = os.path.join(args.data_dir, src)
            img = PIL.Image.open(local_fpath)
            lisa_project["files"].append(
                {
                    "fid": "%d" % fid,
                    "src": src,
                    "regions": [],
                    "fdata": {
                        "height": img.height,
                        "width": img.width,
                    },
                    "rdata": [],
                }
            )

    json.dump(lisa_project, sys.stdout)
    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
